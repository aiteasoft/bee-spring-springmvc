/*
 * Copyright 2016-2021 the original author.All rights reserved.
 * Kingstar(honeysoft@126.com)
 * The license,see the LICENSE file.
 */

package com.automvc.enet.order.rest;

import org.teasoft.honey.osql.autogen.Ddl;

import com.automvc.enet.order.entity.Beeorders;

/**
 * @author Kingstar
 * @since  1.9
 */
public class CreateTable {
	
public static void main(String[] args) {
	Ddl.createTable(new Beeorders());
}
}
