
Bee + Spring + SpringMVC
=========

### 更快的开发Java Web的新组合：Bee+Spring+SpringMVC  
开发速度快并不是PHP和Rails的专利，Java也可以。    
发现Hiberante（MyBatis）+Spring+SpringMVC的开发速度不够快，要编写的代码比较多。    
究其原因，是因为编码复杂度是O(n)。    
**使用Bee+Spring+SpringMVC组合,开发速度快、编码少**，    
不但可以节约软件开发时间，还可以节省软件开发成本；开发速度和开发成本都不比PHP和Rails差。    
这一新组合编码具有省时/优雅、简易、自动(Timesaving/Tasteful, Easy, Automatic) 的Tea风格特征。    

## 快速开发实例：  
### 用Bee+Spring+SpringMVC+easyui,半个小时实现一个历史订单管理功能。  
#### 请看以下截图：  

<img src="JavaWeb-system-images/1.png" width="600">
<img src="JavaWeb-system-images/0.png" width="600">
<img src="JavaWeb-system-images/2.png" width="600">
<img src="JavaWeb-system-images/3.png" width="600">
<img src="JavaWeb-system-images/4.png" width="600">
<img src="JavaWeb-system-images/5.png" width="600">
<img src="JavaWeb-system-images/6.png" width="600">
<img src="JavaWeb-system-images/7.png" width="600">


### Bee是一个 ORM框架,它的开发速度快，编码少,还很简单。    
**Bee** 是一个具有省时/优雅、简易、自动( **Tea:** Timesaving/Tasteful, Easy, Automatic) 风格的ORM框架。  
你还在为不断重复写ORM操作数据库的代码而烦恼吗?每写一个service业务层，就要写一个dao层(即使我们知道dao就是简单的查改增删suid操作)。  
请试试**Bee** 吧，让它帮你从烦琐的编码工作中解脱出来。  
Bee是一种更接近SQL语言思维的ORM框架，  
一种开发速度和开发成本都不比php差的Java ORM框架，  
而且它的编码复杂度是O(1)，即用了Bee，你可以不用另外再写dao代码。  

## Bee主要功能特点介绍：  
**Bee概念简单**(10分钟即可入门)、功能强大。  
Bee **简化了与DB交互的编码**工作量.连接，事务都可以由Bee框架负责管理。  
### 省时,开发速度快
#### 简单易用
* 1.**接口简单，使用方便**。Suid接口中对应SQL语言的select,update,insert,delete操作提供**4个同名方法**。  
* 2.使用了Bee,你可以不用再另外编写dao代码，直接调用Bee的api即可完成对DB的操作。  
* 3.**约定优于配置**:Javabean没有注解,也不需要xml映射文件,只是纯的Javabean即可,甚至get,set方法不用也可以。  
* 4.**智能化自动过滤**null和空字符串，不再需要写判断非空的代码。  
* 5.支持**只查询一部分字段**。   
#### 自动,功强强大
* 6.**动态/任意组合**查询条件,不需要提前准备dao接口,有新的查询需求也不用修改或添加接口。  
* 7.支持原生SQL排序, **原生语句分页**(不需要将全部数据查出来)。  
* 8.支持**直接返回Json**格式查询结果; 链式编程。  
* 9.支持**事务**、多个ORM操作使用同一连接、**for update**，支持**批处理**操作，支持原生SQL(**自定义sql**语句)，支持**存储过程**。
* 10.支持面向对象方式复杂查询、**多表查询**(无n+1问题; 支持：一对一,一对多,多对一,多对多)。 
* 11.**一级缓存**，概念简单，功能强大；一级缓存也可以**像JVM一样进行细粒度调优**；**智能缓存**，支持更新配置表，**不用重启**。  
* 12.表名与实体名、字段名与属性名映射默认提供多种实现，且支持**自定义映射规则扩展**。  
* 13.**多种DB**支持轻松扩展(MySQL,MariaDB,Oracle,H2,SQLite,PostgreSQL,SQL Server等直接可用)。 
* 14.支持**读写分离**一主多从, 仅**分库**等**多数据源**模式(对以前的代码无需修改,该功能对代码是透明的,即无需额外编码),**可同时使用多种类型数据库**。  
* 15.**分布式**环境下生成**连续单调递增**(在一个workerid内),**全局唯一**数字**id**；提供自然简单的分布式主键生成方式。  
* 16.**支持同库分表,动态表名映射**。  
* 17.可以不用表对应的Javabean也能操作DB。  
* 18.**无**第三方插件依赖；可零配置使用。  
* 19.**性能好:接近JDBC的速度；文件小：Bee V1.8 jar 仅217k**, **V1.9.5 jar,仅315k**。  
辅助功能:  
* 20.支持自动生成表对应的Javabean，根据Javabean创建表，Javaweb**后端代码**根据**模板自动生成**；能打印非占位符的**可执行sql**,方便调试。  
* 21.支持**读取Excel**,从Excel导入数据到DB，操作简单。 


**Bee** 网址:  
https://github.com/automvc/bee  
**Bee在gitee** 的网址:  
https://gitee.com/automvc/bee


#### 作者的电子邮箱email:    aiteasoft@126.com  
